## What is this? ##

This java program runs a lisp interpreter over email.
Once its running, you can send it lisp expressions and it will
reply with the result of evaluating those expressions.

![lisp-over-email in action](screenshot.PNG)

The underlying lisp implementation is [ajl](https://bitbucket.org/tflynn/lisp).

## Compilation ##

To compile it you will need ajl, and the javax.mail package on  your CLASSPATH, and run

    $ export CLASSPATH=/usr/share/java/ajl.jar:/usr/share/java/javax.mail.jar:.
    $ javac monitor.java

## Configuration ##
Then, update the file `email.properties` with your email info (imap server, username, password, etc..)

## Launching ##
    $ export CLASSPATH=/usr/share/java/ajl.jar:/usr/share/java/javax.mail.jar:.
    $ java monitor

Ctrl+C to exit.
