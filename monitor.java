/*
  Copyright 2017 Thomas Flynn
*/

import java.util.*;
import java.io.*;
import javax.mail.*;
import javax.mail.event.*;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.InternetAddress;
import javax.mail.Message.RecipientType;
import javax.activation.*;

import com.sun.mail.imap.*;
import com.sun.mail.smtp.*;
import org.ajl.Env;
import org.ajl.fn_vals;
import org.ajl.lisp;
import org.ajl.sym_vals;
import org.ajl.std_lisp;
/* Monitors given mailbox for new mail */

interface MsgHandler{
	public Message handle_message(Message msg_in);
}

abstract class SimpleHandler implements MsgHandler{
	public Message handle_message(Message msg_in){
		String txt = mailutil.get_text(msg_in);
		String ret = handle_message_simple(txt);
		return mailutil.mk_reply(msg_in,ret);
	}
	public abstract String handle_message_simple(String txt);
}

class MsgApp{
	MsgHandler handler;
	Properties props;
	Session session;

	//list of the "to" field on incoming message
  //used to avoid responding to self. Also can block emails
	List<String> bypass_list;
	  
	void do_session(){
		bypass_list=new ArrayList<String>();
		bypass_list.add((String)props.get("mail.address"));
		System.out.println("Bypassing the address "
											 + bypass_list.get(0)
											 + ".");
		try{
	    Store store = session.getStore("imap");
	    store.connect((String)props.get("mail.imaps.host"),
										(String)props.get("mail.imaps.user"),
										(String)props.get("mail.imaps.pass"));
	    Folder folder
				= store.getFolder((String)props.get("mail.imaps.folder"));
	    if (folder == null || !folder.exists()) {
				System.out.println("Invalid folder");
				System.exit(1);
	    }
	    folder.open(Folder.READ_WRITE);
	    
	    folder.addMessageCountListener(new MessageCountAdapter() {
					public void messagesAdded(MessageCountEvent ev) {
						Message[] msgs = ev.getMessages();
		       
						for (int i = 0; i < msgs.length; i++) {
							System.out.println("1. Handling a new msg");

							Boolean bypass=false;
							try{
								bypass =
									mailutil.should_bypass(bypass_list,
																				 msgs[i].getFrom());
							}catch(Throwable e){
								e.printStackTrace();
							}

							if(bypass == true) continue;
							Message ret = handler.handle_message(msgs[i]);
							if(!(ret==null))
								send_message(props,ret);
						}
					}
				});
	    while(true){
				IMAPFolder f = (IMAPFolder)folder;
				f.idle();
	    }
		}catch(Throwable e){
	    System.out.println("Caught Throwable!!!");
	    e.printStackTrace();
		}
	
	}
    
	MsgApp(Properties _props,
				 MsgHandler _handler){
	
		handler = _handler;
		props = _props;
		session = Session.getInstance(props, null);
		while(true)
	    do_session();
	}
	void send_message(Properties props,
										Message ret){
		Session session = Session.getInstance(props,null);

		try{
			SMTPTransport trans =
				(SMTPTransport)session.getTransport("smtps");
			trans.connect((String)props.get("mail.smtps.host"),
										(String)props.get("mail.smtps.user"),
										(String)props.get("mail.smtps.pass"));
			trans.sendMessage(ret,ret.getAllRecipients());
		}
		catch(Throwable e){
	    e.printStackTrace();
		}
	}
}

class lisp_handler extends SimpleHandler{
	std_lisp stdl = new std_lisp();
	String res="";
	
	public class print extends fn_vals{
		public sym_vals fn_val(LinkedList<sym_vals> args){
			System.out.println("Here?");
			String str="";
			ListIterator<sym_vals> valsIt = args.listIterator();
			while(valsIt.hasNext()){
				str += valsIt.next().str_val() + " ";
			}
			res += str + '\n';
			System.out.println(" Response length now "
												 + res.length());
    
			return lisp.lisp_true;
		}
	
	}
	public lisp_handler(){
		//env = lisp.standard_env();
		stdl.env.put("print",new print());
		//thep = new std_parse();
	}
	
	public String handle_message_simple(String txt){
		System.out.println("Contents: " + txt);
		res="";
		String finalres = stdl.runProg_nothrow(txt);
		res = res + finalres;
		System.out.println("2. Response length is "
											 + res.length());
    
		return res;
	}
}

class mailutil{//mail convenience fns
	public static Boolean contains_email(List<String> bypass_list,
																			 String add){
		for(String to_bypass : bypass_list)
			if(add.contains(to_bypass))
				return true;
		return false;
	}
	public static Boolean should_bypass(List<String> bypass_list,
																			Address[] adds){
		Boolean bypass=false;
		for(Address address : adds)
			if(contains_email(bypass_list,address.toString())){
				System.out.println("Bypassing");
				bypass=true;
			}
			else{
				System.out.println("Not bypassing."
													 + address.toString()
													 + ".");
			}
		return bypass;
		
	} 
	public static String get_text(Message msg){
		try{
	    Object content = msg.getContent();
	    if(content instanceof MimeMultipart){
				MimeMultipart mmp = (MimeMultipart)content;
				BodyPart bp = mmp.getBodyPart(0);
				if(bp instanceof MimeBodyPart){
					MimeBodyPart mbp = (MimeBodyPart)bp;
					Object pc = mbp.getContent();
					if(pc instanceof String){
						return (String)pc;
					}
				}
	    }
	    else if(content instanceof String){
				return (String)content;
	    }
	    else
				System.out.println("message was not mimemultipart or string");
		}
		catch(Throwable e){
	    e.printStackTrace();
		}
		return null;
		
	}
	public static Message mk_reply(Message msg, String txt,String subj){
		try{
	    Message ret = msg.reply(false);
	    ret.setFrom(msg.getRecipients(RecipientType.TO)[0]);
	    ret.setSubject(subj);
	    ret.setText(txt);
	    Address[] adds = msg.getFrom();
	    for (Address address : adds)
				System.out.println(address);
	    System.out.println("3. The reply is made");
	    return ret;
		}
		catch(Throwable e){
	    e.printStackTrace();
		}
		return null;
	}
	public static Message mk_reply(Message msg,String txt){
		return mk_reply(msg,txt,"");
	}
}

public class monitor {

	public static void main(String argv[]) {
	
		System.out.println("\nTesting monitor\n");

		try {
	    Properties props = new Properties(); //System.getProperties();

			InputStream input = null;

			try {
				input = new FileInputStream("email.properties");
				props.load(input);
			}
			catch(IOException ex){
				ex.printStackTrace();
			}
			finally{
				MsgApp ma = new MsgApp(props,new lisp_handler());
			}
		}catch(Throwable e){
	    e.printStackTrace();
		}
		return;   
	}
}
